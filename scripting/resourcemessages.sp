#include <sourcemod>
#include <sdktools>
#include <clientprefs>
#undef REQUIRE_PLUGIN
#include <updater>

#pragma semicolon 1

#define RESOURCE_MESSAGES_UPDATE_URL	 "http://vaskrist.source-code.my/nd-plugin-resource-messsages/raw/master/updatefile.txt"
#define RESOURCE_MESSAGES_PLUGIN_URL	 "http://vaskrist.source-code.my/nd-plugin-resource-messsages"
#define RESOURCE_MESSAGES_PLUGIN_NAME    "Resource Messages"
#define RESOURCE_MESSAGES_PLUGIN_VERSION "1.0.2"

/*
* TODO:
*  - group coherent stuff into enums
*  - compute at least central area size (CENTRAL_DISTANCE) based on map size/furthest resource/entity coordinates
*  - do some unit tests for the plugin, like checking the directions discerning, or calculating the center of the map other than the primary
*/
// teams
enum Team
{
	UNASSIGNED,
	SPECTATE,
	TEAM_CON,
	TEAM_EMP,
	TEAM_COUNT
}

enum ResType
{
	PRIMARY,
	SECONDARY,
	TERTIARY
}

// messages display
new bool:show_messages = true;

// menu integration
new Handle:cookie_resmsg = INVALID_HANDLE;
new bool:option_resmsg[MAXPLAYERS + 1] = {false,...};

// resource information
const MAX_RESOURCES = 30;
new resCount = 0;
new resEnts[MAX_RESOURCES] = {0, ...};
new bool:resCapturers[MAX_RESOURCES][MAXPLAYERS + 1];

new teamEnts[TEAM_COUNT];

// reference points and distances
new rotation = 0;
new Float:posCenter[3];
new Float:posBase[TEAM_COUNT][3];
const Float:CENTRAL_DISTANCE = 2000.0; // TODO: set according to map size - for metro cca 2000 so that the west secondary is not considered central but cca 3000 for maps like downtown
const Float:BASE_DISTANCE = 2500.0;

const NAME_SIZE = 64;

public Plugin:myinfo = 
{
	name = RESOURCE_MESSAGES_PLUGIN_NAME,
	author = "Vaskrist",
	description = "Messages about resource capture with !settings menu integration.",
	version = RESOURCE_MESSAGES_PLUGIN_VERSION,
	url = RESOURCE_MESSAGES_PLUGIN_URL
}

/* Core Includes */
#include "resourcemessages/api.sp"
#include "resourcemessages/functions.sp"

public OnLibraryAdded(const String:name[])
{
	if (StrEqual(name, "updater"))
	{
		Updater_AddPlugin(RESOURCE_MESSAGES_UPDATE_URL);
	}
}

public OnPluginStart()
{
	// menu integration + messages
	LoadTranslations("resourcemessages.phrases"); //basic translation support is a must for Redstone
	LoadTranslations("common.phrases"); //required for on and off
	
	cookie_resmsg = RegClientCookie("Resource Messages On/Off", "", CookieAccess_Protected); 
	new info;
	SetCookieMenuItem(CookieMenuHandler_resmsg, any:info, "Resource Messages");
	
	// if we get reloaded during a map, we need to read all the client cookies right away
	for (new client = 1; client <= MaxClients; client++)
	{
		if (IsValidClient(client))
		{
			OnClientCookiesCached(client);
		}
	}
	
	// resource capture handling
	HookEvent("resource_start_capture", Event_ResourceStartCapture, EventHookMode_Post);
	HookEvent("resource_end_capture", Event_ResourceEndCapture, EventHookMode_Post);
	HookEvent("resource_captured", Event_ResourceCaptured, EventHookMode_Post);
	
	HookEvent("round_start", Event_RoundStart, EventHookMode_Post);
	HookEvent("enter_pregame", Event_EnterPregame, EventHookMode_Post);
	
	if (LibraryExists("updater"))
	{
		Updater_AddPlugin(RESOURCE_MESSAGES_UPDATE_URL);
	}
	
	PrintToServer("Plugin %s started successfully.", RESOURCE_MESSAGES_PLUGIN_NAME);
}

public OnMapStart()
{
	// get reference points for area computations
	GetFirstEntityOrigin("nd_info_primary_resource_point", posCenter);
	GetFirstEntityOrigin("nd_info_command_bunker_ct",  posBase[TEAM_CON]);
	GetFirstEntityOrigin("nd_info_command_bunker_emp", posBase[TEAM_EMP]);
	
	// get the team entities
	teamEnts[TEAM_CON] = FindEntityByClassname(-1, "nd_team_consortium");
	teamEnts[TEAM_EMP] = FindEntityByClassname(-1, "nd_team_empire");
	
	// reset all capturing and resource info
	resCount = 0;
	for (new resIndex = 0; resIndex < MAX_RESOURCES; resIndex++)
	{
		resEnts[resIndex] = 0;
		ClearCapturers(resIndex);
	}
	
	// read map compass_rotation from the map script file
	rotation = ReadMapCompassRotation();
}

// ======== EVENT HANDLING ========
public Event_EnterPregame(Handle:event, const String:name[], bool:dontBroadcast)
{
	// don't show messasges during pregame
	show_messages = false;
}

public Event_RoundStart(Handle:event, const String:name[], bool:dontBroadcast)
{
	// now we can start showing messages
	show_messages = true;
}

public Event_ResourceStartCapture(Handle:event, const String:name[], bool:dontBroadcast)
{
	new userid = GetEventInt(event, "userid");
	new entindex = GetEventInt(event, "entindex");
	
	// convert values for local use
	new client = GetClientOfUserId(userid);
	new resIndex = GetResIndex(entindex);

	if (IsValidClient(client, false))
	{
		// add userid to the list for entindex
		AddCapturer(resIndex, client);
	}
	
}

public Event_ResourceEndCapture(Handle:event, const String:name[], bool:dontBroadcast)
{
	new userid = GetEventInt(event, "userid");
	new entindex = GetEventInt(event, "entindex");
	
	// convert values for local use
	new client = GetClientOfUserId(userid);
	new resIndex = GetResIndex(entindex);
	
	// remove user from the list of players capturing the resource
	RemoveCapturer(resIndex, client);
}

public Event_ResourceCaptured(Handle:event, const String:name[], bool:dontBroadcast)
{
	new entindex = GetEventInt(event, "entindex");
	new type = GetEventInt(event, "type");
	new team = GetEventInt(event, "team");
	
	if (team != _:TEAM_EMP && team != _:TEAM_CON) return;
	
	// convert values for local use
	new resIndex = GetResIndex(entindex);
	
	// get all people who were capturing
	decl String:capsNames[MAXPLAYERS+1][NAME_SIZE];
	new capsCount = GetCapturerNames(resCapturers[resIndex], team, capsNames, (type == _:PRIMARY) ? 2 : 1);

	// send a message to all the people in the team
	if (show_messages && capsCount > 0)
	{
		// prepare message
		decl String:nameString[MAXPLAYERS*NAME_SIZE];
		ImplodeStrings(capsNames, capsCount, ",", nameString, sizeof(nameString));
		
		// get area/direction of the resource 
		new Float:pos[3], String:direction[3];
		GetEntPropVector(entindex, Prop_Send, "m_vecOrigin", pos);
		direction = GetDirection(pos, team, type);
		
		// get basic resource captured key
		new String:resKey[32];
		switch (ResType:type)
		{
			case PRIMARY:   resKey = "Primary Resource Captured";
			case SECONDARY: resKey = "Secondary Resource Captured";
			case TERTIARY:  resKey = "Tertiary Resource Captured";
		}
		
		for (new client = 1; client <= MaxClients; client++)
		{
			
			if (IsValidClient(client) && option_resmsg[client] && GetClientTeam(client) == team)
			{
				// translate area
				decl String:areaKey[32], String:area[48], String:message[512];
				Format(areaKey, sizeof(areaKey), "Map Area %s", direction);
				Format(area, sizeof(area), "%T", areaKey, client);
				
				// translate message
				Format(message, sizeof(message), "\x03%T", resKey, client, nameString, area);
				
				// show it to the user
				PrintToChat(client, message);
			}
		}
	}
	
	// drop all info on that resource
	ClearCapturers(resIndex);
}

// ======== COOKIE HANDLING + MENU ========
/// Cookie menu handler
public CookieMenuHandler_resmsg(client, CookieMenuAction:action, any:info, String:buffer[], maxlen)
{
	if (action == CookieMenuAction_DisplayOption)
	{
		decl String:status[10];
		if (option_resmsg[client])
		{
			Format(status, sizeof(status), "%T", "On", client);
		}
		else
		{
			Format(status, sizeof(status), "%T", "Off", client);
		}
		
		Format(buffer, maxlen, "%T: %s", "Cookie Resource Messages", client, status);
	}
	// CookieMenuAction_SelectOption
	else
	{
		option_resmsg[client] = !option_resmsg[client];
		
		if (option_resmsg[client])
		{
			SetClientCookie(client, cookie_resmsg, "On");
			PrintToChat(client, "\x04Resource Messages Enabled!");
		}
		else
		{
			SetClientCookie(client, cookie_resmsg, "Off");
			PrintToChat(client, "\x04Resource Messages Disabled!");
		}
		
		ShowCookieMenu(client);
	}
}

/// Cookie caching in array
public OnClientCookiesCached(client)
{
	option_resmsg[client] = GetCookie_resmsg(client);
}

/// Reading cookies with default being Off
bool:GetCookie_resmsg(client)
{
	decl String:buffer[10];
	GetClientCookie(client, cookie_resmsg, buffer, sizeof(buffer));
	
	// if not explicitly set to "On" use "Off" by default
	return StrEqual(buffer, "On");
}

// ======== LOCAL FUNCTIONS ========
/// Returns the index of the resource in the static field resEnts or -1 if there are more resources than the constant 
GetResIndex(entIndex)
{
	// iterate through the resource array and return the index
	for (new resIndex = 0; resIndex < MAX_RESOURCES; resIndex++)
	{
		if (resEnts[resIndex] == entIndex)
		{
			return resIndex;
		}
	}
	
	// if there is no such resource, just add a new one and 
	if (resCount < MAX_RESOURCES)
	{
		new resIndex = resCount;
		resCount++;
		
		resEnts[resIndex] = entIndex;
		ClearCapturers(resIndex);
		return resIndex;
	}
	
	return -1;
}

/// Clears the flags of capturing players for the resource
ClearCapturers(resIndex)
{
	// clear capturers for the resource
	for (new client = 1; client <= MaxClients; client++)
	{
		resCapturers[resIndex][client] = false;
	}
}

/// Adds a capturer to the resource
AddCapturer(resIndex, client)
{
	resCapturers[resIndex][client] = true;
}

/// Adds a capturer from the resource
RemoveCapturer(resIndex, client)
{
	resCapturers[resIndex][client] = false;
}

/// Returns a comma separated list of capturers from the team, limited by maxNames, in the names String
GetCapturerNames(bool:clients[], team, String:names[MAXPLAYERS+1][NAME_SIZE], maxNames)
{
	new nameCount = 0;
	new capCount = 0;
	
	// get human capturer names for the resource
	for (new client = 1; client <= MaxClients; client++)
	{
		
		if (IsValidClient(client, false) && clients[client] && GetClientTeam(client) == team && !IsFakeClient(client))
		{
			decl String:name[NAME_SIZE];
			GetClientName(client, name, sizeof(name));
			
			if (capCount < maxNames)
			{
				names[nameCount] = name;
				nameCount++;
			}
			capCount++;
		}
	}
	
	// get robot capturer names for the resource
	for (new client = 1; client <= MaxClients; client++)
	{
		
		if (IsValidClient(client, false) && clients[client] && GetClientTeam(client) == team && IsFakeClient(client))
		{
			decl String:name[NAME_SIZE];
			GetClientName(client, name, sizeof(name));
			
			if (capCount < maxNames)
			{
				// here I could put bot names in parenthesis
				names[nameCount] = name;
				nameCount++;
			}
			capCount++;
		}
	}
	
	// if there were more capturers than we care to display, add the surplus (+X) to the last name
	if (nameCount > 0 && capCount > nameCount)
	{
		// add +X to the last name
		decl String:appdendedname[NAME_SIZE];
		Format(appdendedname, sizeof(appdendedname), "%s +%d", names[nameCount-1], capCount - nameCount);
		names[nameCount-1] = appdendedname;
	}
	
	return nameCount;
}

/// Checks if the client is valid and maybe not a bot
stock bool:IsValidClient(client, bool:nobots = true)
{
	if (client <= 0 || client > MaxClients || !IsClientConnected(client) || (nobots && IsFakeClient(client)))
	{
		return false;
	}
	return IsClientInGame(client);
}

/// Checks if the position is closer to the teams posBase than the global constant
bool:IsCloseToBase(Float:pos[3], team)
{
	return IsCloseTo(pos, posBase[team], BASE_DISTANCE);
}

/// Checks if the position is closer to the reference point than the specified distance
bool:IsCloseTo(Float:pos[3], Float:ref[3], Float:dist)
{
	return GetVectorDistance(pos, ref) < dist;
}

/**
 * Returns resources cardinal direction (N, NW, ... ) from center,
 * or own base (OB), enemy base (EB), or central area (CE)
 * according to the position, team and resource type.
 * 
 * Is used in translation key later.
 */
String:GetDirection(Float:pos[3], team, type)
{
	new String:direction[3];
	
	// for primary there is no direction
	if (type == _:PRIMARY) { direction = "CE"; return direction; }
	
	new Float:rel[3];
	SubtractVectors(pos, posCenter, rel);
	
	new otherteam = _:TEAM_CON + _:TEAM_EMP - team;
	if (IsCloseToBase(pos, team)) { direction = "OB"; return direction; }
	if (IsCloseToBase(pos, otherteam)) { direction = "EB"; return direction; }
	if (type == _:TERTIARY && GetVectorLength(pos) < CENTRAL_DISTANCE) { direction = "CE"; return direction; }
	
	new ns = 0, we = 0;
	new bS = -180, bW = -90, bN = 0, bE = 90;
	new Float:R = 60.0;
	new Float:S = float(((bS + 180 + rotation) % 360)-180);
	new Float:W = float(((bW + 180 + rotation) % 360)-180);
	new Float:N = float(((bN + 180 + rotation) % 360)-180);
	new Float:E = float(((bE + 180 + rotation) % 360)-180);
	
	new Float:angle = RadToDeg(ArcTangent2(rel[0],rel[1]));
	if ( IsInBounds(angle, N-R,  N+R)) ns = 3; // NORTH
	if ( IsInBounds(angle, S-R,  S+R)) ns = 6; // SOUTH
	if (!IsInBounds(angle, W+R, -W-R)) we = 1; // WEST
	if ( IsInBounds(angle, E-R,  E+R)) we = 2; // EAST

/*	
	new Float:S = float(((bS + 180 + rotation) % 360)-180);
	new Float:W = float(((bW + 180 + rotation) % 360)-180);
	new Float:N = float(((bN + 180 + rotation) % 360)-180);
	new Float:E = float(((bE + 180 + rotation) % 360)-180);
	
	new Float:angle = RadToDeg(ArcTangent2(rel[0],rel[1]));
	if ( IsInBounds(angle, N-R,  N+R)) ns = 3; // NORTH
	if ( IsInBounds(angle, S-R,  S+R)) ns = 6; // SOUTH
	if (!IsInBounds(angle, W+R, -W-R)) we = 1; // WEST
	if ( IsInBounds(angle, E-R,  E+R)) we = 2; // EAST
*/
	
	new comb = ns + we;
	switch(comb) {
		case 0: direction = "CE";
		case 1: direction = "W";
		case 2: direction = "E";
		case 3: direction = "N";
		case 4: direction = "NW";
		case 5: direction = "NE";
		case 6: direction = "S";
		case 7: direction = "SW";
		case 8: direction = "SE";
	}
	
	return direction;
}

/// Checks if the value is inside the bounds
bool:IsInBounds(Float:value, Float:lowerBound, Float:upperBound)
{
	return lowerBound < value && value < upperBound;
}

/// Fills passed vector with m_vecOrigin of first entity found by class
GetFirstEntityOrigin(String:class[], Float:pos[3])
{
	new entindex = FindEntityByClassname(-1, class);
	if (entindex != -1)
	{
		GetEntPropVector(entindex, Prop_Send, "m_vecOrigin", pos);
	}
}

/// Reads compass rotation of the current map from the map script file
ReadMapCompassRotation()
{
	new String:mapName[32];
	GetCurrentMap(mapName, sizeof(mapName));

	return ReadCompassRotation(mapName);
}


