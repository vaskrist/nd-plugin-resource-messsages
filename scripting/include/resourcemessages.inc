#if defined _resourcemessages_included
  #endinput
#endif
#define _resourcemessages_included

/**
 * @brief Returns the total amount of resources gathered by the team from round start.
 *
 * @param team	the team to check the value for (2 for CON, 3 for EMP)
 * @return amount of resources
 */
native GetResourcesTotal(team);

/**
 * @brief Returns the total amount of resources expended by the team from round start.
 *
 * @param team	the team to check the value for (2 for CON, 3 for EMP)
 * @return amount of resources
 */
native GetResourcesExpended(team);

/**
 * @brief Returns the total amount of resources currently held by the team
 *
 * @param team	the team to check the value for (2 for CON, 3 for EMP)
 * @return amount of resources
 */
native GetResourcesAmount(team);
