/** Testable functions separation. */

/// Reads compass rotation of the given map
ReadCompassRotation(String:mapName[])
{
	new result = 0;
	
	new String:filePath[64];
	Format(filePath, sizeof(filePath), "scripts/maps/%s.txt", mapName);
	
	new Handle:kv = CreateKeyValues(mapName);
	if (FileToKeyValues(kv, filePath))
	{
		result = KvGetNum(kv, "compass_rotation", 0);
		LogMessage("Read compass_rotation = %d from map script file of '%s'", result, mapName);
	}
	else
	{
		LogError("ReadMapCompassRotation - could not read KeyValue file: %s holding script for map: %s.", filePath, mapName);
	}
	CloseHandle(kv);

	return result;
}