/* API - Natives & Forwards */
public APLRes:AskPluginLoad2(Handle:myself, bool:late, String:error[], err_max)
{
	CreateNative("GetResourcesTotal",    Native_GetResourcesTotal);
	CreateNative("GetResourcesExpended", Native_GetResourcesExpended);
	CreateNative("GetResourcesAmount",   Native_GetResourcesAmount);
	
	return APLRes_Success;
}

/// Returns total rources for the given team as the sum of expended and actual resources
// native native GetResourcesTotal(team);
public Native_GetResourcesTotal(Handle:plugin, numParams)
{
	// retrieve team parameter
	new team = GetNativeCell(1);
	
	// check team value
	if (team != _:TEAM_CON && team != _:TEAM_EMP)
	{
		return ThrowNativeError(SP_ERROR_NATIVE, "Invalid team (%d). Accepting only 2 for CON, 3 for EMP.", team);
	}
	
	// get the team entity
	new teament = teamEnts[team];
	if (teament != -1)
	{
		new amount = GetEntProp(teament, Prop_Send, "m_iResourcePoints");
		new expended = GetEntProp(teament, Prop_Send, "m_iExpendedResources");
		
		return amount + expended;
	}
	else
	{
		return -1;
	}
}

/// Returns expended rources for the given team
// native GetResourcesExpended(team);
public Native_GetResourcesExpended(Handle:plugin, numParams)
{
	// retrieve team parameter
	new team = GetNativeCell(1);
	
	// check team value
	if (team != _:TEAM_CON && team != _:TEAM_EMP)
	{
		return ThrowNativeError(SP_ERROR_NATIVE, "Invalid team (%d). Accepting only 2 for CON, 3 for EMP.", team);
	}
	
	// get the team entity
	new teament = teamEnts[team];
	if (teament != -1)
	{
		new expended = GetEntProp(teament, Prop_Send, "m_iExpendedResources");
		
		return expended;
	}
	else
	{
		return -1;
	}
}

/// Returns actual rources for the given team
// native GetResourcesAmount(team);
public Native_GetResourcesAmount(Handle:plugin, numParams)
{
	// retrieve team parameter
	new team = GetNativeCell(1);
	
	// check team value
	if (team != _:TEAM_CON && team != _:TEAM_EMP)
	{
		return ThrowNativeError(SP_ERROR_NATIVE, "Invalid team (%d). Accepting only 2 for CON, 3 for EMP.", team);
	}
	
	// get the team entity
	new teament = teamEnts[team];
	if (teament != -1)
	{
		new amount = GetEntProp(teament, Prop_Send, "m_iResourcePoints");
		
		return amount;
	}
	else
	{
		return -1;
	}
}
